require 'yb/ecommerce/version'
require 'yb/ecommerce/configuration'
require 'yb/ecommerce/youbiquity_api'

require 'yb/ecommerce/resource'
require 'yb/ecommerce/identity'
require 'yb/ecommerce/interaction'
