module Yb
  module Ecommerce
    class Resource
      class FailedToCreateIdentity < Exception; end
      class FailedToVinculateIdentity < Exception; end
      class FailedToUpdateIdentity < Exception; end
      class FailedToCreateInteraction < Exception; end

      private

      def call_api_post(path, body)
        YoubiquityApi.post(
          "#{Yb::Ecommerce.config.base_uri}/#{path}",
          body: body.merge(api_key: Yb::Ecommerce.config.api_key).to_json,
          headers: {'Content-Type' => 'application/json'})
      end

      def call_api_patch(path, body)
        YoubiquityApi.patch(
          "#{Yb::Ecommerce.config.base_uri}/#{path}",
          body: body.merge(api_key: Yb::Ecommerce.config.api_key).to_json,
          headers: {'Content-Type' => 'application/json'})
      end
    end
  end
end
