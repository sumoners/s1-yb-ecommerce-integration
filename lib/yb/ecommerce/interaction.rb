require 'httparty'

module Yb
  module Ecommerce
    class Interaction < Yb::Ecommerce::Resource
      def create(action, fingerprint, metadata = {}, raw_data = {},
                 time = Time.now)

        data = {
          data_source_id: Yb::Ecommerce.config.data_source_id,
          type: 'EcommerceInteraction',
          identity: {
            fingerprint: fingerprint,
            type: 'EcommerceTrackerIdentity',
            information: metadata.delete(:identity_information)
          },
          i_action: action,
          qualifier: nil,
          metadata: metadata,
          raw_data: raw_data,
          interaction_time: time
        }

        response = call_api_post('interactions', data)

        unless [200,201].include?(response.code)
          raise FailedToCreateInteraction, response.body
        end

        response
      rescue Errno::ECONNREFUSED => e
        raise FailedToCreateInteraction, e.message
      end
    end
  end
end
