module Yb
  module Ecommerce
    def self.configure(&block)
      yield @config ||= Yb::Ecommerce::Configuration.new
    end

    def self.config
      @config
    end

    class Configuration
      attr_accessor :api_key, :service_identifier, :base_uri, :data_source_id

      def initialize
        self.api_key = ''
        self.service_identifier = ''
        self.base_uri = ''
        self.data_source_id = ''
      end
    end
  end
end
