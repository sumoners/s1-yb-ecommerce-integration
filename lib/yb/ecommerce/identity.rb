require 'httparty'

module Yb
  module Ecommerce
    class Identity < Yb::Ecommerce::Resource
      # Create a new identity
      # fingerprint = a identifier unique, ex: id
      # informations = all informations about client, ex: email, address...
      #
      def create(user_id, type, information = {}, identifier = '')
        identifier = (identifier == '') ? 
          "ecommerce_#{Yb::Ecommerce.config.data_source_id}" :
          identifier

        params_hash = {
          fingerprint: user_id,
          type: type,
          service_identifier: identifier,
          information: information }

        response = call_api_post('identities', params_hash)

        unless [200,201].include?(response.code)
          raise FailedToCreateIdentity, response.body
        end

        response
      rescue Errno::ECONNREFUSED => e
        raise FailedToCreateIdentity, e.message
      end

      # Updates an existing identity
      #
      # It's pretty standard, much like creation.
      def update(user_id, type, information = {})
        params_hash = {
          type: type,
          identity: {
            information: information
          }
        }

        response = call_api_patch("identities/#{user_id}", params_hash)

        unless [200,201].include?(response.code)
          raise FailedToUpdateIdentity, response.body
        end

        response
      end

      # Vinculate a user with a another services, ex: twitter, facebook...
      def vinculate(fingerprint_a, type_a, fingerprint_b, type_b)
        params_hash = {
          identity_a: { fingerprint: fingerprint_a, type: type_a },
          identity_b: { fingerprint: fingerprint_b, type: type_b },
          service_identifier: Yb::Ecommerce.config.service_identifier,
          api_key: Yb::Ecommerce.config.api_key
        }

        response = call_api_post('identity_relations', params_hash)

        unless [200,201].include?(response.code)
          raise FailedToVinculateIdentity, response.body
        end

        response
      rescue Errno::ECONNREFUSED => e
        raise FailedToVinculateIdentity, e.message
      end
    end
  end
end
