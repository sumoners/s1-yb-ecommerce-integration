# Yb::Ecommerce::Integration

Gem for integrate your ecommerce with Awesome Youbiquity :metal:

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'yb-ecommerce-integration', git: 'git@github.com:sumoners/s1-yb-ecommerce-integration.git', branch: 'master'
```

And then execute:

    $ bundle

## Usage

### Setup
```ruby
require 'yb/integration'

Yb::Ecommerce.configure do |c|
  c.api_key = '1a874b9b24d9343b8eb2387014dbb2da71bc1bcc'
  c.base_uri = 'http://127.0.0.1:3000/api/v1'
  c.data_source_id = 12
end
```

### Create a new Ecommerce Identity
```ruby
@identity = Yb::Ecommerce::Identity.new
# Informations is optional
@identity.create('14', 'EcommerceTrackerIdentity', informations)
```

|Code|Definition                                             |
|----|-------------------------------------------------------|
|201 |Sucess                                                 |
|422 |Problem on save identity, see response for more details|

### Vinculate two identities
```ruby
@identity = Yb::Ecommerce::Identity.new

@identity.create(fingerprint_a, type_a, fingerprint_b, type_b)
```

|Code|Definition                                             |
|----|-------------------------------------------------------|
|201 |Sucess                                                 |
|422 |Problem on vinculate, see response for more details    |
