require 'spec_helper'

describe Yb::Ecommerce::Interaction, :vcr do
  let(:interaction) { subject }

  describe '#create' do
    it 'creates an interaction if everything goes well' do
      response = interaction.create(
        "ecommerce:user:login",
        1234)

      expect(response.code).to eq(201)
    end

    it 'raises an exception if the response is not 200 or 201' do
      stub_request(:post, /.*127.*/).to_return(
        body: "erro!", status: 500)

      expect(-> { interaction.create('a','1') }).to raise_exception(
        Yb::Ecommerce::Identity::FailedToCreateInteraction)
    end

    it 'raises an exception if the connection fails' do
      stub_request(:post, /.*127.*/).to_raise(Errno::ECONNREFUSED)

      expect(-> { interaction.create('a','1') }).to raise_exception(
        Yb::Ecommerce::Identity::FailedToCreateInteraction)
    end
  end
end
