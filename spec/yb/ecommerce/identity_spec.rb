#encoding: utf-8
require 'spec_helper'

describe Yb::Ecommerce::Identity, :vcr do
  before do
    @identity = Yb::Ecommerce::Identity.new
  end

  describe 'identity creation' do
    it 'creates a new ecommerce identity when everything goes right' do
      response = @identity.create('14', 'EcommerceTrackerIdentity')
      expect(response.code).to eq(201)
    end

    it 'raises an exception if the response is not 200 or 201' do
      stub_request(:post, /.*127.*/).to_return(
        body: "erro!", status: 500)

      expect(-> { @identity.create('1','x') }).to raise_exception(
        Yb::Ecommerce::Identity::FailedToCreateIdentity)
    end

    it 'raises an exception if the connection fails' do
      stub_request(:post, /.*127.*/).to_raise(Errno::ECONNREFUSED)

      expect(-> { @identity.create('a','1') }).to raise_exception(
        Yb::Ecommerce::Identity::FailedToCreateIdentity)
    end
  end

  describe 'identity update' do
    it 'updates an existing ecommerce identity with given data' do
      first_re = @identity.create('14', 'EcommerceTrackerIdentity',
                                  {'name' => 'Lucas Bronson'})

      response = @identity.update('14',
                                  { 'name' => 'João da Bega' })

      expect(response.code).to eq(200)
      expect(JSON.parse(response.body)['updated_at']).to_not eq(
        JSON.parse(first_re.body)['updated_at'])
    end

    it 'fails if the customer does not exist on yb' do
      expect( -> { @identity.update('mang08asndui', {}) }).to raise_exception(
        Yb::Ecommerce::Identity::FailedToUpdateIdentity)
    end
  end

  describe 'identity vinculation' do
    it 'vinculates two identities' do
      identity_a = @identity.create('255', 'EcommerceTrackerIdentity')
      identity_b = @identity.create('258', 'EcommerceTrackerIdentity')

      response = @identity.vinculate(
        identity_a['fingerprint'],
        identity_a['type'],
        identity_b['fingerprint'],
        identity_b['type'])

      expect(response.code).to eq(201)
    end

    it 'raises an exception if the response is not 200 or 201' do
      stub_request(:post, /.*127.*/).to_return(
        body: "erro!", status: 500)

      expect(-> { @identity.vinculate(1,2,3,4) }).to raise_exception(
        Yb::Ecommerce::Identity::FailedToVinculateIdentity)
    end

    it 'raises an exception if the connection fails' do
      stub_request(:post, /.*127.*/).to_raise(Errno::ECONNREFUSED)

      expect(-> { @identity.vinculate(1,2,3,4) }).to raise_exception(
        Yb::Ecommerce::Identity::FailedToVinculateIdentity)
    end
  end
end
