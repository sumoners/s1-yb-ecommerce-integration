require 'simplecov'

SimpleCov.start do
  add_filter 'vendor'
  add_filter 'spec'
  # If coverage is < 95%, simplecov exits with non-zero
  minimum_coverage 95

  # CircleCI integration - Inserts coverage reports into ARTIFACTS directory
  if ENV['CIRCLE_ARTIFACTS']
    coverage_dir File.join(
      '..', '..', '..', ENV['CIRCLE_ARTIFACTS'], 'coverage')
  end
end

require 'yb/integration'
require 'webmock/rspec'
require 'pry'
require 'vcr'

VCR.configure do |config|
  config.cassette_library_dir = 'spec/cassettes'
  config.hook_into :webmock
  config.configure_rspec_metadata!
end

RSpec.configure do |config|
  config.before(:suite) do
    Yb::Ecommerce.configure do |c|
      c.api_key = 'ef4cb18c56759fc57d8dcf8ecda49014ea81c30f'
      c.service_identifier = 'ecommerce_1'
      c.base_uri = 'http://127.0.0.1:3001/api/v1'
      c.data_source_id = 1
    end
  end
end
