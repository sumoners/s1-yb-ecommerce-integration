# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'yb/ecommerce/version'

Gem::Specification.new do |spec|
  spec.name          = "yb-ecommerce-integration"
  spec.version       = Yb::Ecommerce::VERSION
  spec.authors       = ["Sumoner"]
  spec.email         = ["contact@sumone.com.br"]
  spec.summary       = %q{'Gem for integrate your ecommerce with Awesome Youbiquity'}
  spec.description   = %q{'Gem for integrate your ecommerce with Awesome Youbiquity'}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'vcr'
  spec.add_development_dependency 'webmock'
  spec.add_development_dependency 'pry'

  spec.add_dependency 'httparty', '~> 0.11.0'
end
